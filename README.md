# DLM3054 WDF file read

1. install Matlab
    - version > 2019b (not sure)  
2. Drag `WDFAccess.miappinstall` to matlab and add install path to Configure Paths
![](https://raw.githubusercontent.com/waterver/PicGo/2023/1703054317045.jpg)
3. download and install GCC-W64:https://github.com/jmeubank/tdm-gcc/releases/download/v10.3.0-tdm64-2/tdm64-gcc-10.3.0-2.exe
(e.g. `default install path`:`C:\TDM-GCC-64`)  
4. add gcc to system  
    - set gcc path to system (`vale name`:`MW_MINGW64_LOC`;`vale`:`path`)  
![set gcc path](https://raw.githubusercontent.com/waterver/PicGo/2023/20231220142506.png)  
    - add `exe` folder to system (`%MW_MINGW64_LOC%\bin`)  
![](https://raw.githubusercontent.com/waterver/PicGo/2023/20231220142941.png) 
5. run `wdf_starup.m`
